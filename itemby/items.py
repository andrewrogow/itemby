import scrapy


class ProfileItem(scrapy.Item):
    table = scrapy.Field()
    title = scrapy.Field()
    age = scrapy.Field()
    height = scrapy.Field()
    wight = scrapy.Field()
    email = scrapy.Field()
    phone = scrapy.Field()
    description = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
