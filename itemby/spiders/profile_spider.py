import scrapy
from scrapy.http import Request

from itemby.items import ProfileItem

ROOT_URL = 'https://in.timby.cc'

class ProfileSpider(scrapy.Spider):
    exclude_items = ['/', None, '/add.html', '/search.html', '/login.html', '/forgotpassword.html', '/datingrules.html', 'http://www.yandex.ru/cy?base=0&host=intimby.info']
    root_url = ROOT_URL
    name = 'profile_spider'
    start_urls = [f'{ROOT_URL}/cgi-bin/select.pl?Gender=woman&Orientation=any&penpal=2&friendship=2&flirt=2&marriage=2&sponsor=2&money=2&City=%CC%E8%ED%F1%EA&newcity=&AgeMin=18&AgeMax=40&Social=any&Added=any&OrderBy=datepublished']

    @staticmethod
    def get_total_pages(table):
        count = table.xpath('tr[1]/td[2]//text()').extract()[8].split()[-1]
        return int(count)

    @staticmethod
    def calculate_page(page):
        pass

    def parse(self, response, **kwargs):
        profile_table = response.xpath('/html/body/table[3]')
        total_pages = self.get_total_pages(profile_table) # 100

        # open each page and scrapy
        urls = (self.start_urls[0] + f'&Start={page}' for page in range(0, total_pages, 20))
        for url in urls:
            yield Request(url, callback=self.parse_page)

    def parse_page(self, response):
        profile_table = response.xpath('/html/body/table[3]')
        rows = profile_table.xpath('//tr')

        for row in rows:
            uri = row.xpath('td/a/@href').extract_first()
            if uri in self.exclude_items:
                continue

            yield Request(self.root_url + uri, callback=self.parse_profile_page)

    def parse_profile_page(self, response):
        table = response.xpath('/html/body/table[3]')
        title = table.xpath('tr[1]/td[2]/table/tr[2]/td[2]//text()').extract_first()
        age = table.xpath('tr[1]/td[2]/table/tr[4]/td[2]//text()').extract_first()
        height = table.xpath('tr[1]/td[2]/table/tr[10]/td[2]//text()').extract_first()
        wight = table.xpath('tr[1]/td[2]/table/tr[11]/td[2]//text()').extract_first()
        email = table.xpath('tr[1]/td[2]/table/tr[14]/td[2]//text()').extract_first()
        phone = self.get_phone(table)
        description = table.xpath('tr[1]/td[2]/table/tr[17]//text()').extract()
        photo_link = self.get_photo_link(table)

        payload = {
            'title': title,
            'age': age,
            'height': height,
            'wight': wight,
            'email': email,
            'phone': phone,
            'description': description,
            'image_urls': photo_link,
        }
        print('+++++++++++++++++++++++++++', payload)
        yield ProfileItem(**payload)

    def get_photo_link(self, table):
        photo_link = table.xpath('tr[1]/td[2]/center/img//@src').extract_first()
        if photo_link:
            return [self.root_url + photo_link]
        return []


    @staticmethod
    def get_phone(table):
        if table.xpath('tr[1]/td[2]/table/tr[15]/td[1]//text()').extract_first() == 'Дата публикации:':
            return
        return table.xpath('tr[1]/td[2]/table/tr[15]/td[2]//text()').extract_first()






# /html/body/table[3]/tbody/tr[1]/td[2]/table[1]/

#rows[5].xpath('td/a/@href').extract_first()
#rows = t.xpath('//tr')
#t = response.xpath('/html/body/table[3]')
